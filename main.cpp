#include <cstdint>

#include <memoryapi.h>

struct address_t {
	std::uintptr_t value = 0;

	constexpr address_t( std::uintptr_t address ) : value( address ) {}

	template<typename T> operator T *() const { return reinterpret_cast<T *>( value ); }
};

static constexpr address_t addresses[] = {
	// Main vehicles
	0x00733275,
	0x00733FB6,
	// Trains
	0x00733365,
	0x00734276,
	// Boats
	0x0073357E,
	0x007344D1,
	// Big vehicles
	0x00733453,
	0x007343A6
};

static constexpr std::uint8_t patch_code = 0xEB;
static constexpr std::uint8_t orig_code = 0x7A;

struct loader {
	loader() {
		for ( auto address : addresses ) write( address, patch_code );
	}
	~loader() {
		for ( auto address : addresses ) write( address, orig_code );
	}

	template<typename T> static void write( address_t address, T value ) {
		auto orig_vp = set_protect( address, sizeof( T ), PAGE_EXECUTE_READWRITE );
		*static_cast<T *>( address ) = value;
		set_protect( address, sizeof( T ), orig_vp );
	}

	static DWORD set_protect( address_t address, DWORD size, DWORD new_protect ) {
		DWORD oldVP;
		VirtualProtect( address, size, new_protect, &oldVP );
		return oldVP;
	}
} loader;