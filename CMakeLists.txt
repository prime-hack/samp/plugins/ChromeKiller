cmake_minimum_required(VERSION 3.18.1)

project(ChromeKiller DESCRIPTION "Disable any chrome-effects on cars in GTA:SA" LANGUAGES CXX)

aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR} SOURCE_FILES)

add_library(${PROJECT_NAME} MODULE ${SOURCE_FILES})

set_target_properties(${PROJECT_NAME} PROPERTIES
                      CXX_STANDARD 20
                      CXX_STANDARD_REQUIRED YES
                      CXX_EXTENSIONS NO
                      CXX_VISIBILITY_PRESET hidden
                      C_VISIBILITY_PRESET hidden
                      VISIBILITY_INLINES_HIDDE ON
                      PREFIX ""
                      SUFFIX ".asi"
)

configure_file(.clangd.in ${CMAKE_SOURCE_DIR}/.clangd @ONLY) 
